from transformers import TapasTokenizer, TFTapasForQuestionAnswering
import pandas as pd
import datetime


def execute_query(query, csv_file, max_sequence_length=None):
    a = datetime.datetime.now()

    # Read the CSV file in chunks or sample a subset
    if max_sequence_length:
        tokenizer = TapasTokenizer.from_pretrained("google/tapas-base-finetuned-wtq", max_length=max_sequence_length)
    else:
        tokenizer = TapasTokenizer.from_pretrained("google/tapas-base-finetuned-wtq")

    chunk_size = 64
    table_chunks = pd.read_csv(csv_file.name, delimiter=",", chunksize=chunk_size)
    table = pd.concat(table_chunks)

    # Fill NaN values (if required) and convert to string
    table.fillna(0, inplace=True)
    table = table.astype(str)
    model = TFTapasForQuestionAnswering.from_pretrained("google/tapas-base-finetuned-wtq")

    queries = [query]

    inputs = tokenizer(table=table, queries=queries, padding="max_length", return_tensors="tf")
    outputs = model(**inputs)

    predicted_answer_coordinates, predicted_aggregation_indices = tokenizer.convert_logits_to_predictions(
        inputs, outputs.logits, outputs.logits_aggregation
    )
# Let's print out the results:
    id2aggregation = {0: "NONE", 1: "SUM", 2: "AVERAGE", 3: "COUNT"}
    aggregation_predictions_string = [id2aggregation[x] for x in predicted_aggregation_indices]

    answers = []
    for coordinates in predicted_answer_coordinates:
        if len(coordinates) == 1:
            # Only a single cell
            answers.append(table.iat[coordinates[0]])
        else:
            # Multiple cells
            cell_values = []
            for coordinate in coordinates:
                cell_values.append(table.iat[coordinate])
            answers.append(cell_values)
    
    for query, answer, predicted_agg in zip(queries, answers, aggregation_predictions_string):
        if predicted_agg != "NONE":
            answers.append(predicted_agg)

    query_result = {
        "query": query,
        "result": answers
    }
    print(query_result)

    b = datetime.datetime.now()
    print(b - a)

    return query_result

def execute_query(query, csv_file):
    a = datetime.datetime.now()

    model_name = "google/tapas-base-finetuned-wtq"
    model = TFTapasForQuestionAnswering.from_pretrained(model_name)
    tokenizer = TapasTokenizer.from_pretrained(model_name)

    queries = [query]

    max_rows = 160
    max_columns = 32

    answers = []

    with open(csv_file.name, 'r') as file:
        table_reader = pd.read_csv(file, delimiter=',', chunksize=max_rows)
        for chunk_table in table_reader:
            chunk_table.fillna(0, inplace=True)
            chunk_table = chunk_table.astype(str)

            for column_index in range(0, len(chunk_table.columns), max_columns):
                start_column = column_index
                end_column = min(column_index + max_columns, len(chunk_table.columns))

                subtable = chunk_table.iloc[:, start_column:end_column]

                inputs = tokenizer(table=subtable, queries=queries, padding="max_length", return_tensors="tf")
                outputs = model(**inputs)

                predicted_answer_coordinates, predicted_aggregation_indices = tokenizer.convert_logits_to_predictions(
                    inputs, outputs.logits, outputs.logits_aggregation
                )

                id2aggregation = {0: "NONE", 1: "SUM", 2: "AVERAGE", 3: "COUNT"}
                aggregation_predictions_string = [id2aggregation[x] for x in predicted_aggregation_indices]

                for coordinates in predicted_answer_coordinates:
                    if len(coordinates) == 1:
                        # only a single cell:
                        answers.append(subtable.iat[coordinates[0]])
                    else:
                        # multiple cells
                        cell_values = []
                        for coordinate in coordinates:
                            cell_values.append(subtable.iat[coordinate])
                        answers.append(cell_values)

                for query, answer, predicted_agg in zip(queries, answers, aggregation_predictions_string):
                    if predicted_agg != "NONE":
                        answers.append(predicted_agg)

    query_result = {
        "query": query,
        "result": answers
    }
    print(query_result)

    b = datetime.datetime.now()
    print(b - a)

    return query_result





# from transformers import TapasTokenizer, TFTapasForQuestionAnswering
# import pandas as pd
# import datetime


# def execute_query(query, csv_file):
#     a = datetime.datetime.now()

#     table = pd.read_csv(csv_file.name, delimiter=",")
#     table.fillna(0, inplace=True)
#     table = table.astype(str)

#     model_name = "google/tapas-base-finetuned-wtq"
#     model = TFTapasForQuestionAnswering.from_pretrained(model_name)
#     tokenizer = TapasTokenizer.from_pretrained(model_name)

#     queries = [query]

#     inputs = tokenizer(table=table, queries=queries, padding="max_length", return_tensors="tf")
#     outputs = model(**inputs)

#     predicted_answer_coordinates, predicted_aggregation_indices = tokenizer.convert_logits_to_predictions(
#         inputs, outputs.logits, outputs.logits_aggregation
#     )

#     # let's print out the results:
#     id2aggregation = {0: "NONE", 1: "SUM", 2: "AVERAGE", 3: "COUNT"}
#     aggregation_predictions_string = [id2aggregation[x] for x in predicted_aggregation_indices]

#     answers = []
#     for coordinates in predicted_answer_coordinates:
#         if len(coordinates) == 1:
#             # only a single cell:
#             answers.append(table.iat[coordinates[0]])
#         else:
#             # multiple cells
#             cell_values = []
#             for coordinate in coordinates:
#                 cell_values.append(table.iat[coordinate])
#             answers.append(cell_values)

#     for query, answer, predicted_agg in zip(queries, answers, aggregation_predictions_string):
#         if predicted_agg != "NONE":
#             answers.append(predicted_agg)

#     query_result = {
#         "query": query,
#         "result": answers
#     }
#     print(query_result)

#     b = datetime.datetime.now()
#     print(b - a)

#     return query_result, table
