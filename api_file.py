import gradio as gr
import pandas as pd
import psycopg2
import environ
from langchain import OpenAI, SQLDatabase, SQLDatabaseChain

env = environ.Env()
environ.Env.read_env()

API_KEY = env('OPENAI_API_KEY')

# Setup database
db = SQLDatabase.from_uri(
    f"postgresql+psycopg2://postgres:{env('DBPASS')}@localhost:5432/{env('DATABASE')}",
)

# Setup language model
llm = OpenAI(temperature=0, openai_api_key=API_KEY)

# Create database chain
QUERY = """
Given an input question, first create a syntactically correct PostgreSQL query to run, then look at the results of the query and return the answer.
Use the following format:

Question: {question}
SQLQuery: {sql_query}
SQLResult: Result of the SQLQuery
Answer: Final answer here
"""

# Setup the database chain
db_chain = SQLDatabaseChain(llm=llm, database=db, verbose=True)


def connect_to_database():
    conn = psycopg2.connect(
        host='localhost',
        port=5432,
        user='postgres',
        password=env('DBPASS'),
        database=env('DATABASE')
    )
    return conn


def create_table_from_csv(file):
    # Read the CSV file
    data = pd.read_csv(file.name)

    # Establish a connection to the PostgreSQL database
    conn = connect_to_database()
    cursor = conn.cursor()

    # Get the column names and types from the CSV file
    columns = data.columns
    column_types = data.dtypes

    # Generate the CREATE TABLE query dynamically
    create_table_query = f"CREATE TABLE IF NOT EXISTS dynamic_table ("
    for col, col_type in zip(columns, column_types):
        if col_type == 'object':
            create_table_query += f'"{col}" TEXT,'
        elif col_type == 'int64':
            create_table_query += f'"{col}" INTEGER,'
        elif col_type == 'float64':
            create_table_query += f'"{col}" REAL,'
    create_table_query = create_table_query[:-1] + ")"

    # Execute the CREATE TABLE query
    cursor.execute(create_table_query)
    conn.commit()

    cursor.close()
    conn.close()

    return "Table created successfully."

def process_input_with_sql_query(file, question):
    try:
        # Create table from the CSV file
        create_table_from_csv(file)

        # Generate the SQL query using the database chain
        sql_query = db_chain.run(question)
        # return sql_query
        # sql = sql_query.get("SQLQuery")
        # print(sql)  # Print the SQL query in the VS Code terminal

        # Execute the SQL query against the database
        conn = connect_to_database()
        cursor = conn.cursor()
        cursor.execute(sql_query)
        result = cursor.fetchall()
        cursor.close()
        conn.close()

        # Format the SQL query and result
        # sql_query_str = f"SQL Query:\n{sql_query}"
        # result_str = f"SQL Result:\n{result}"
        

        return result

    except Exception as e:
        return str(e), ""


# Gradio interface
input_file = gr.inputs.File(label="Upload CSV File")
input_question = gr.inputs.Textbox(label="Enter your question")

# output_sql_query= gr.outputs.Textbox(label="SQL Query",type='text')
output_result = gr.outputs.Textbox(label="result",type='text')

iface = gr.Interface(
    fn=process_input_with_sql_query,
    inputs=[input_file, input_question],
    outputs=output_result,
    title="Question Answering with CSV",
)

iface.launch(share=True)
