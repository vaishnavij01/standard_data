# import streamlit as st
# import pandas as pd
# import plotly_express as px

# def clean_data(data):
#     # Add your data cleaning logic here
#     # For example, removing missing values or duplicates
#     cleaned_data = data.drop_duplicates().dropna()
#     return cleaned_data

# def main():
#     # Page layout
#     st.set_page_config(page_title='Data Processing App', layout='centered')

#     # Logo
#     st.image('logo.jpg', width=300)

#     if "cleaned_data" not in st.session_state:
#         st.session_state.cleaned_data = None

#     # File Upload
#     st.write('Upload a CSV file for analysis:')
#     uploaded_file = st.file_uploader("Choose a file", type="csv")
#     if uploaded_file is not None:
#         df = pd.read_csv(uploaded_file)

#         st.subheader('Original Data')
#         st.write(df.head())

#         # Data Cleaning Button
#         if st.button('Data Cleaning'):
#             cleaned_df = clean_data(df)
#             st.subheader('Cleaned Data')
#             st.write(cleaned_df.head())

#             # Store cleaned data in session state
#             st.session_state.cleaned_data = cleaned_df

#     if st.session_state.cleaned_data is not None:
#         cleaned_df = st.session_state.cleaned_data

#         # Data Visualization
#         st.subheader("Data Visualization")

#         # Get numeric and categorical columns
#         numeric_columns = list(cleaned_df.select_dtypes(include=['float', 'int']).columns)
#         categorical_columns = list(cleaned_df.select_dtypes(include=['object']).columns)

#         # Add a select widget to choose the chart type
#         chart_select = st.selectbox(
#             label="Select the chart type",
#             options=['Scatterplot', 'Lineplot', 'Barplot', 'Boxplot', 'Piechart']
#         )

#         if chart_select == "Scatterplot":
#             st.subheader("Scatterplot Settings")
#             x_values = st.selectbox('X axis', options=numeric_columns)
#             y_values = st.selectbox('Y axis', options=numeric_columns)
#             plot = px.scatter(data_frame=cleaned_df, x=x_values, y=y_values)

#             # Display The Scatter Plot
#             st.plotly_chart(plot)

#         elif chart_select == 'Lineplot':
#             st.subheader('Line Plot settings')
#             x_values = st.selectbox('X Axis', options=categorical_columns)
#             y_values = st.selectbox('Y Axis', options=numeric_columns)
#             plot = px.line(data_frame=cleaned_df, x=x_values, y=y_values)

#             st.plotly_chart(plot)

#         elif chart_select == 'Barplot':
#             st.subheader('Bar Plot settings')
#             x_values = st.selectbox('X Axis', options=categorical_columns)
#             y_values = st.selectbox('Y Axis', options=numeric_columns)
#             plot = px.bar(data_frame=cleaned_df, x=x_values, y=y_values)

#             st.plotly_chart(plot)

#         elif chart_select == 'Piechart':
#             st.subheader('Pie Chart Settings')
#             names = st.selectbox('names', options=categorical_columns)
#             values = st.selectbox('values', options=numeric_columns)
#             plot = px.pie(cleaned_df, values=values, names=names)

#             st.plotly_chart(plot)

#         elif chart_select == 'Boxplot':
#             st.subheader('Box Plot settings')
#             x_values = st.selectbox('X Axis', options=categorical_columns)
#             y_values = st.selectbox('Y Axis', options=numeric_columns)
#             plot = px.box(data_frame=cleaned_df, x=x_values, y=y_values)

#             st.plotly_chart(plot)


# if __name__ == "__main__":
#     main()



import streamlit as st
import pandas as pd
import plotly_express as px

def clean_data(data):
    cleaned_data = data.drop_duplicates().dropna()
    return cleaned_data

def main():
    # Page layout
    st.set_page_config(page_title='Data Processing App', layout='centered')

    # Logo
    st.image('logo.jpg', width=100)

    if "cleaned_data" not in st.session_state:
        st.session_state.cleaned_data = None

    # File Upload
    st.write('Upload a CSV file for analysis:')
    uploaded_file = st.file_uploader("Choose a file", type="csv")
    if uploaded_file is not None:
        df = pd.read_csv(uploaded_file)

        st.subheader('Original Data')
        st.write(df.head())

        # Data Cleaning Button
        if st.button('Data Cleaning'):
            cleaned_df = clean_data(df)
            #st.subheader('Cleaned Data')
            #st.write(cleaned_df.head())

            # Store cleaned data in session state
            st.session_state.cleaned_data = cleaned_df

    if st.session_state.cleaned_data is not None:
        cleaned_df = st.session_state.cleaned_data

        # Data Visualization
        st.subheader("Data Visualization")

        # Display cleaned data
        st.write('Cleaned Data:')
        st.write(cleaned_df)

        # Get numeric and categorical columns
        numeric_columns = list(cleaned_df.select_dtypes(include=['float', 'int']).columns)
        categorical_columns = list(cleaned_df.select_dtypes(include=['object']).columns)

        # Add a select widget to choose the chart type
        chart_select = st.selectbox(
            label="Select the chart type",
            options=['Scatterplot', 'Lineplot', 'Barplot', 'Boxplot', 'Piechart']
        )

        if chart_select == "Scatterplot":
            st.subheader("Scatterplot Settings")
            x_values = st.selectbox('X axis', options=categorical_columns)
            y_values = st.selectbox('Y axis', options=numeric_columns)
            plot = px.scatter(data_frame=cleaned_df, x=x_values, y=y_values)
                
            # Set y-axis range
            plot.update_yaxes(range=[0, 50])
    
            # Display The Scatter Plot
            st.plotly_chart(plot)

        elif chart_select == 'Lineplot':
            st.subheader('Line Plot settings')
            x_values = st.selectbox('X Axis', options=categorical_columns)
            y_values = st.selectbox('Y Axis', options=numeric_columns)
            plot = px.line(data_frame=cleaned_df, x=x_values, y=y_values)

            # Set y-axis range
            plot.update_yaxes(range=[0, 50])
            st.plotly_chart(plot)

        elif chart_select == 'Barplot':
            st.subheader('Bar Plot settings')
            x_values = st.selectbox('X Axis', options=categorical_columns)
            y_values = st.selectbox('Y Axis', options=numeric_columns)
            plot = px.bar(data_frame=cleaned_df, x=x_values, y=y_values)

            # Set y-axis range
            #plot.update_yaxes(range=[0, 1000])

            st.plotly_chart(plot)

        elif chart_select == 'Piechart':
            st.subheader('Pie Chart Settings')
            names = st.selectbox('names', options=categorical_columns)
            values = st.selectbox('values', options=numeric_columns)
            plot = px.pie(cleaned_df, values=values, names=names)

            # Set y-axis range
            plot.update_yaxes(range=[0, 50])
            st.plotly_chart(plot)

        elif chart_select == 'Boxplot':
            st.subheader('Box Plot settings')
            x_values = st.selectbox('X Axis', options=categorical_columns)
            y_values = st.selectbox('Y Axis', options=numeric_columns)
            plot = px.box(data_frame=cleaned_df, x=x_values, y=y_values)
            
            # Set y-axis range
            plot.update_yaxes(range=[0, 50])
            st.plotly_chart(plot)


if __name__ == "__main__":
    main()
